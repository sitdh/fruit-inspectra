//
//  main.m
//  FruitDBInspectra
//
//  Created by Sitdhibong Laokok on 16/11/56.
//  Copyright (c) พ.ศ. 2556 Apps Tree. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FDBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FDBAppDelegate class]));
    }
}
