//
//  FDBAppDelegate.h
//  FruitDBInspectra
//
//  Created by Sitdhibong Laokok on 16/11/56.
//  Copyright (c) พ.ศ. 2556 Apps Tree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FDBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
